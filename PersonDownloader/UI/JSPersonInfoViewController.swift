//
//  ViewController.swift
//  PersonDownloader
//
//  Created by Jerzy Świniarski on 12/07/16.
//  Copyright © 2016 JS. All rights reserved.
//

import UIKit
import SDWebImage

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}

class JSPersonInfoViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sexLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel?
    
    @IBOutlet weak var nameValueLabel: UILabel!
    @IBOutlet weak var sexValueLabel: UILabel!
    @IBOutlet weak var ageValueLabel: UILabel?
    
    @IBOutlet weak var personImageView: UIImageView!
    @IBOutlet weak var refreshButton: UIButton!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.commonSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - Setup methods
    func commonSetup () {
        self.setupLabels()
        self.setupIndicator()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(personInfoChanged),
                                               name: NSNotification.Name(rawValue: Constants.Notifications.PersonInfoChangedKey),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(errorReceived),
                                               name: NSNotification.Name(rawValue: Constants.Notifications.Errors.ConnectivityErrorKey),
                                               object: nil)
    }
    
    func setupLabels() {
        nameLabel.text = "nameKey".localized + ":"
        sexLabel.text = "sexKey".localized + ":"
        if (ageLabel != nil) {
            ageLabel!.text = "ageKey".localized + ":"
        }
        refreshButton.setTitle("refresh", for: UIControlState())
        
        nameValueLabel.text = ""
        sexValueLabel.text = ""
        if (ageLabel != nil) {
            ageValueLabel!.text = ""
        }
    }
    
    func setupIndicator() {
        activityIndicator.hidesWhenStopped = true
        activityIndicator.stopAnimating()
    }
    
    // MARK: - Actions
    @IBAction func refreshButtonPressed(_ sender: AnyObject) {
        DLog("refreshButtonPressed")
        JSConnectivityManager.sharedInstance.downloadNewPersonInfo()
        activityIndicator.startAnimating()
    }
    
    // MARK: - Delegate and notification methods
    func personInfoChanged (_ notification:Notification) {
        print("personInfoChanged")
        if let newName = notification.userInfo?[Constants.PersonInfoKeys.NameKey] as? String {
            //UImodifications on main thread
            DispatchQueue.main.async(execute: {
                self.nameValueLabel.text = newName
            })
        }
        if let newSex = notification.userInfo?[Constants.PersonInfoKeys.SexKey] as? String {
            //UImodifications on main thread
            DispatchQueue.main.async(execute: {
                self.sexValueLabel.text = newSex
            })
            
        }
        if let newAge = notification.userInfo?[Constants.PersonInfoKeys.AgeKey] as? Int {
            //UImodifications on main thread
            DispatchQueue.main.async(execute: {
                if (self.ageValueLabel != nil) {
                    self.ageValueLabel!.text = "\(newAge)"
                }
            })
        }
        
        if let newImageAddress = notification.userInfo?[Constants.PersonInfoKeys.ImageAddressKey] as? String {
            //mixing model with UI a bit, but wanted to use library for loading images
            let url = URL(string: newImageAddress)
            
            self.personImageView.sd_setImage(with: url,
                                             placeholderImage: UIImage(named:"person_placeHolder"),
                                             options: SDWebImageOptions(),
                                             completed: {(Bool)  in
                                                DispatchQueue.main.async(execute: {
                                                    self.activityIndicator.stopAnimating()
                                                })
            })
        }
    }
    
    func errorReceived (_ notification:Notification) {
        DispatchQueue.main.async(execute: {
            self.activityIndicator.stopAnimating()
            if let errorMessage = notification.userInfo?[Constants.Notifications.Errors.ErrorMessageKey] as? String {
                let alert = UIAlertController(title: "ERROR:", message: errorMessage, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        })
    }
}

