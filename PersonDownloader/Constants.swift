//
//  Constants.swift
//  PersonDownloader
//
//  Created by Jerzyk on 7/15/16.
//  Copyright © 2016 JS. All rights reserved.
//


struct Constants {
    struct Notifications {
        static let PersonInfoChangedKey = "personInfoChangedNotification"
        struct Errors {
            static let ErrorMessageKey = "errorMessage"
            static let ConnectivityErrorKey = "ConnectivityError"
        }
        
    }
    struct PersonInfoKeys {
        static let NameKey = "personInfoName"
        static let AgeKey = "personInfoAge"
        static let SexKey = "personInfoSex"
        static let ImageAddressKey = "personInfoImageAddress"
    }
    
    struct JSONPersonInfoKeys {
        static let ResultsKey = "results"
        static let GenderKey = "gender"
        static let AgeKey = "results"
        static let NameKey = "name"
        static let ImageKey = "picture"
        
        struct ImageKeys {
            static let LargeKey = "large"
            static let MediumKey = "medium"
            static let ThumbnailKey = "thumbnail"
        }
        struct NameKeys {
            static let FirstKey = "first"
            static let LastKey = "last"
        }
        static let PictureKey = "picture"
    }
}
