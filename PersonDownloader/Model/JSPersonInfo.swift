//
//  JSPerson.swift
//  PersonDownloader
//
//  Created by Jerzyk on 7/15/16.
//  Copyright © 2016 JS. All rights reserved.
//

import Foundation

class JSPersonInfo {
    var age: Int = 0
    var name = "noname"
    var sex = "nosex" //TODO: could've used enum for that
    var imageAddress = "" 
    
    var personInfoDict : NSDictionary {
        get {
            let dict = [Constants.PersonInfoKeys.NameKey            :   name,
                        Constants.PersonInfoKeys.SexKey             :   sex,
                        Constants.PersonInfoKeys.AgeKey             :   age,
                        Constants.PersonInfoKeys.ImageAddressKey    :   imageAddress] as [String : Any]
            return dict as NSDictionary
        }
    }
    
    convenience init(aName: String, anAge: Int, aSex: String) {
        self.init()
        name = aName
        age = anAge
        sex = aSex
    }
    
    //TODO: add other initializers
}
