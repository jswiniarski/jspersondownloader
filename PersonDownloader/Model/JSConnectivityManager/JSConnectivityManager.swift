//
//  JSConnectivityManager.swift
//  PersonDownloader
//
//  Created by Jerzyk on 7/15/16.
//  Copyright © 2016 JS. All rights reserved.
//

import Foundation

protocol JSConnectivityManagerDelegate {
    
}

class JSConnectivityManager : NSObject {
    //singleton creation
    static let sharedInstance = JSConnectivityManager()
    var endpointURL = "http://api.randomuser.me/"
    var urlSession = URLSession.shared
    
    //--------------------
    //MARK: - Init and configuration
    //--------------------
    //hide from public - only sharedInstance to be used
    fileprivate override init() {
        super.init()
    }
    
    
    
    func downloadNewPersonInfo () {
        DLog("\n--------------------------------\nNew person info download started")
        self.requestPersonInfo(endpointURL,
                               responseHandler: {(receivedPersonInfo : JSPersonInfo?) in
                                DLog("receivedPersonInfo: \(receivedPersonInfo)")
                                if (receivedPersonInfo != nil) {
                                    self.postNotificationWithNewPersonInfo(receivedPersonInfo!)
                                }
        })
    }
    
    // MARK: - Networking
    fileprivate func requestPersonInfo(_ endPointURL : String, responseHandler : @escaping (_ personInfo : JSPersonInfo?) -> () ) -> () {
        let url:URL = URL(string: endpointURL)!
        let task = self.urlSession.dataTask(with: url,
                                                   completionHandler: {(data, response, error) in
                                                    if ((error) != nil) {
                                                        //post notification that something went wrong
                                                        DLog("ERROR: \(error!)")
                                                        let message = "Received error: \(error!.localizedDescription)"
                                                        let infoDict = [Constants.Notifications.Errors.ErrorMessageKey: message]
                                                        NotificationCenter.default.post(
                                                            name: Notification.Name(rawValue: Constants.Notifications.Errors.ConnectivityErrorKey),
                                                            object: nil,
                                                            userInfo: infoDict)
                                                        return
                                                    }
                                                    if ((response) != nil) {
                                                        print("Received response:\(response!)")
                                                        if let responseStatus = response as? HTTPURLResponse {
                                                            switch responseStatus.statusCode {
                                                            case 200:
                                                                //parse data only if status 200 received
                                                                if ((data) != nil){
                                                                    let personInfo = self.parseJSONdata(data!)
                                                                    DLog("Received JSON: \(personInfo)")
                                                                    responseHandler(personInfo)
                                                                }
                                                                //TODO: implement common statuses - 400, 404, etc.
                                                            default:
                                                                let message = "Bad status received: \(responseStatus.statusCode)"
                                                                DLog("ERROR" + message)
                                                                let infoDict = [Constants.Notifications.Errors.ErrorMessageKey: message]
                                                                NotificationCenter.default.post(
                                                                    name: Notification.Name(rawValue: Constants.Notifications.Errors.ConnectivityErrorKey),
                                                                    object: nil,
                                                                    userInfo: infoDict)
                                                            }
                                                        }
                                                    }
        })
        
        task.resume()
    }
    
    func parseJSONdata(_ data: Data) -> (JSPersonInfo?) {
        do {
            DLog("Parsing JSON data started")
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String : AnyObject]
            //assuming jsonResult received
            DLog("Parsed JSON: \(jsonResult)")
            let personInfo = JSPersonInfo()
            
            if let results = jsonResult![Constants.JSONPersonInfoKeys.ResultsKey] as? [[String: AnyObject]] {
                DLog("Parsed JSON: \(results)")
                if let name = results[0][Constants.JSONPersonInfoKeys.NameKey] as? [String: AnyObject] { //assuming only 1 result received
                    let lastName = name[Constants.JSONPersonInfoKeys.NameKeys.LastKey] as? String
                    personInfo.name = lastName!
                }
                if let sex = results[0][Constants.JSONPersonInfoKeys.GenderKey] as? String { //assuming only 1 result received
                    personInfo.sex = sex
                }
                if let image = results[0][Constants.JSONPersonInfoKeys.ImageKey] as? [String: AnyObject] { //assuming only 1 result received
                    let imageAddress = image[Constants.JSONPersonInfoKeys.ImageKeys.LargeKey] as? String
                    personInfo.imageAddress = imageAddress!
                }
            }
            return personInfo
        }
            //handle JSON serialisation errors
        catch {
            let message = "parsing JSON: \((error as NSError).localizedDescription)"
            DLog("ERROR:" + message)
            let infoDict = [Constants.Notifications.Errors.ErrorMessageKey: message]
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: Constants.Notifications.Errors.ConnectivityErrorKey),
                object: nil,
                userInfo: infoDict)
            return nil
        }
        
    }
    
    
    //MARK:  - Helper methods
    fileprivate func postNotificationWithNewPersonInfo (_ personInfo : JSPersonInfo) {
        let infoDict = personInfo.personInfoDict
        NotificationCenter.default.post(
            name: Notification.Name(rawValue: Constants.Notifications.PersonInfoChangedKey),
            object: nil,
            userInfo: infoDict as! [String : AnyObject])
    }
}
